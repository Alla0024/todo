<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query = Task::query();

        if ($request->has('status')) {
            $query->byStatus($request->status);
        }

        if ($request->has('priority_from')) {
            $query->where('priority', '>=', $request->priority_from);
        }
        if ($request->has('priority_to')) {
            $query->where('priority', '<=', $request->priority_to);
        }

        if ($request->has('title')) {
            $query->where('title', 'like', '%' . $request->title . '%');
        }

        if ($request->has('sort_by')) {
            $sortField = $request->sort_by;
            $query->orderBy($sortField, $request->has('sort_desc') ? 'desc' : 'asc');
        }

        $tasks = $query->get();

        return response()->json(['tasks' => $tasks]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required',
            'description' => 'nullable',
            'status' => 'in:todo,done',
            'priority' => 'integer|min:1|max:5',
            'user_id' => 'nullable|exists:users,id',
            'parent_id' => 'nullable|exists:tasks,id',
        ]);

        $task = Task::create($data);

        if ($task) {
            return response()->json(['message' => 'Task created successfully']);
        } else {
            return response()->json(['message' => 'Error creating task'], 401);
        }
    }

    /**
     * @param Request $request
     * @param Task $task
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Task $task)
    {
        if ($task->user_id !== Auth::id()) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        $data = $request->validate([
            'title' => 'required',
            'description' => 'nullable',
            'status' => 'in:todo,done',
            'priority' => 'integer|min:1|max:5',
            'completed_at' => 'nullable|date',
        ]);

        $task->update($data);

        return response()->json(['message' => 'Task updated successfully']);
    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Task $task)
    {
        $existingTask = Task::find($task->id);

        if (!$existingTask) {
            return response()->json(['error' => 'Task not found'], 404);
        }

        if ($existingTask->status === 'done') {
            return response()->json(['error' => 'Cannot delete a completed task'], 400);
        }

        if ($existingTask->user_id !== auth()->id()) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        $existingTask->delete();
        return response()->json(['message' => 'Task deleted successfully']);
    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\JsonResponse
     */
    public function complete(Task $task)
    {
        if ($task->hasUncompletedSubtasks()) {
            return response()->json(['error' => 'Cannot complete a task with uncompleted subtasks'], 400);
        }

        if ($task->user_id !== auth()->id()) {
            return response()->json(['error' => 'Unauthorized'], 403);
        }

        $task->update(['status' => 'done']);

        return response()->json(['message' => 'Task completed successfully'], 200);
    }

}
